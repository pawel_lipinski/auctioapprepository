package com.example.AuctionApp.api.model;

import jakarta.validation.constraints.Email; //todo zapytać czy javax czy jakarta
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class Customer {
    private Long id;
    @Email
    private String login;
    private String firstName;
    private String lastName;
    private String password;
    private String role; // todo admin or casual user

}