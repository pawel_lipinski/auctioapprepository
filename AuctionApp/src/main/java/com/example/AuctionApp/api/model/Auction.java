package com.example.AuctionApp.api.model;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Auction {
    private Long id;
    @NotBlank(message = "Proszę podać tytuł aukcji")
    private String title;
    @NotBlank(message = "Proszę podać cenę")
    private Double price;

}