package com.example.AuctionApp.config;

import com.example.AuctionApp.repository.CustomerEntity;
import com.example.AuctionApp.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
@RequiredArgsConstructor
public class SecurityUserDetailsService implements UserDetailsService {

    private final CustomerRepository customerRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        CustomerEntity customerEntity = customerRepository.findByLogin(s)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        return new User(customerEntity.getLogin(), customerEntity.getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + customerEntity.getRole())));
    }
}
