package com.example.AuctionApp.service;

import com.example.AuctionApp.api.model.Customer;
import com.example.AuctionApp.repository.CustomerEntity;
import com.example.AuctionApp.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class CustomerService {
    private final CustomerRepository customerRepository;
    private final PasswordEncoder passwordEncoder;


    public Optional<Customer> findByLogin(String login) {
        return customerRepository.findByLogin(login).map(this::toCustomer);
    }

    public void create(Customer customer) {
        customerRepository.save(CustomerEntity.builder()
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .login(customer.getLogin())
                .password(passwordEncoder.encode(customer.getPassword()))
                .role(customer.getRole())
                .build());
    }

    public void update(Customer customer) {
        customerRepository.save(CustomerEntity.builder()
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .login(customer.getLogin())
                .id(customer.getId())
                .password(customer.getPassword())
                .role(customer.getRole())
                .build());
    }

    public List<Customer> getAll() {
        return customerRepository.findAll().stream()
                .map(this::toCustomer)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        customerRepository.deleteById(id);
    }

    public Customer getById(Long id) {
        return toCustomer(customerRepository.getById(id));
    }

    private Customer toCustomer(CustomerEntity entity) {
        return Customer.builder()
                .id(entity.getId())
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .login(entity.getLogin())
                .password(entity.getPassword())
                .role(entity.getRole())
                .build();
    }
}