package com.example.AuctionApp.web;

import com.example.AuctionApp.api.model.Customer;
import com.example.AuctionApp.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/customer")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping
    @PreAuthorize("isAnonymous()")
    public ModelAndView displayAddCustomerPage() {
        ModelAndView mav = new ModelAndView("addCustomer");
        mav.addObject("customer", new Customer());
        mav.addObject("isEdited", false);
        return mav;
    }

//    dla użytkownika Admin - inaczej wyświetla bład
//    @GetMapping("/edit")
//    public ModelAndView displayEditUserPage(@RequestParam Long id) {
//        ModelAndView mav = new ModelAndView("addUser");
//        mav.addObject("user", userService.getById(id));
//        mav.addObject("isEdited", true);
//        return mav;
//    }


    @PostMapping
    public RedirectView handleAddCustomer(@ModelAttribute("customer") Customer customer) {
        if (customer.getId() == null) {
            customerService.create(customer);
        } else {
            customerService.update(customer);
        }

        return new RedirectView("/");
    }
}
