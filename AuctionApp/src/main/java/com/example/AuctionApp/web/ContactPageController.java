package com.example.AuctionApp.web;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;

@Controller
public class ContactPageController {

    @GetMapping("/kontakt")
    public ModelAndView displayContactPage() {
        ModelAndView mav = new ModelAndView("kontakt");
        mav.addObject("currentDate", LocalDate.now());
        return mav;
    }
}


