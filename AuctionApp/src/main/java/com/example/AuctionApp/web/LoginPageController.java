package com.example.AuctionApp.web;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;

@Controller
public class LoginPageController {

    @GetMapping("/login")
    public ModelAndView displayMainPage() {
        ModelAndView mav = new ModelAndView("login");

        return mav;
    }
}


